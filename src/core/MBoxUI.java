package core;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;

import java.util.Scanner;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 * MBoxUI represents the user interface of the application. It runs a separate
 * thread, reading commands from the console, that display information
 * regarding the application or run different system analysis core.tools.
 */
class MBoxUI implements Runnable {
    private MBox box;

    public MBoxUI(MBox box) {
        this.box = box;
    }

    /**
     * Starts a loop that reads, interprets and executes commands from the
     * console.
     */
    public void startConsole() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("> ");
            String line = scanner.nextLine();
            if (line.compareTo("quit") == 0) {
                break;
            }
        }

        box.terminate();
    }

    public void terminate() {}

    @Override
    public void run() {
        startConsole();
    }
}
