package core.ml;

import burlap.mdp.auxiliary.DomainGenerator;
import burlap.mdp.core.Domain;

/**
 * Created by Alexandru Iulian Orhean on 6/24/16.
 */
public class MLDomain implements DomainGenerator {
    private Integer domainIdentifier;

    public MLDomain(Integer domainIdentifier) {
        this.domainIdentifier = domainIdentifier;
    }

    @Override
    public Domain generateDomain() {
        return null;
    }
}
