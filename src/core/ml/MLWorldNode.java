package core.ml;

import burlap.mdp.core.oo.state.ObjectInstance;
import burlap.mdp.core.state.State;
import burlap.mdp.core.state.UnknownKeyException;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Alexandru Iulian Orhean on 6/24/16.
 */
public class MLWorldNode implements ObjectInstance {
    private int identifier;
    private int precision;
    private int loadLevel;
    private boolean hasParent;
    private boolean hasSibling;
    private String name;

    public static final String CLASS_NAME = "MLWorldNode";
    public static final String INSTANCE_FIRSTNAME = "Node";
    public static final String VAR_LOADLEVEL = "loadLevel";
    public static final String VAR_HASPARENT = "hasParent";
    public static final String VAR_HASSIBLING = "hasSibling";
    private static final List<Object> keys =
            Arrays.<Object>asList(VAR_LOADLEVEL, VAR_HASPARENT, VAR_HASSIBLING);

    public MLWorldNode(int identifier, int precision) {
        this.identifier = identifier;
        this.precision = precision;
        this.loadLevel = 0;
        this.hasParent = false;
        this.hasSibling = false;
        this.name = INSTANCE_FIRSTNAME + this.identifier;
    }

    @Override
    public String className() {
        return CLASS_NAME;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public ObjectInstance copyWithName(String s) {
        MLWorldNode copy = (MLWorldNode) copy();

        copy.setName(s);

        return copy;
    }

    @Override
    public List<Object> variableKeys() {
        return keys;
    }

    @Override
    public Object get(Object o) {
        if (o.equals(VAR_LOADLEVEL)) {
            return loadLevel;
        } else if (o.equals(VAR_HASPARENT)) {
            return hasParent;
        } else if (o.equals(VAR_HASSIBLING)) {
            return hasSibling;
        }
        throw new UnknownKeyException(o);
    }

    @Override
    public State copy() {
        MLWorldNode copy = new MLWorldNode(identifier, precision);

        copy.setLoadLevel(loadLevel);
        copy.setHasParent(hasParent);
        copy.setHasSibling(hasSibling);

        return copy;
    }

    public void setLoadLevel(Integer loadLevel) {
        this.loadLevel = loadLevel;
    }

    public int getLoadLevel() {
        return loadLevel;
    }

    public void setHasParent(boolean hasParent) {
        this.hasParent = hasParent;
    }

    public boolean getHasParent() {
        return hasParent;
    }

    public void setHasSibling(boolean hasSibling) {
        this.hasSibling = hasSibling;
    }

    public boolean getHasSibling() {
        return hasSibling;
    }

    public void setName(String s) {
        name = s;
    }
}
