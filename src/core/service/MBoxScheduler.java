package core.service;

import core.common.MBoxDomain;
import core.common.MBoxSchedInfo;
import core.common.MBoxSchedReq;
import core.common.MBoxSchedRes;

import java.rmi.RemoteException;

/**
 * Created by Alexandru Iulian Orhean on 6/15/16.
 */
public class MBoxScheduler implements MBoxSchedulerInterface {
    private MBoxService service;

    public MBoxScheduler(MBoxService service) {
        this.service = service;
    }

    @Override
    public void register(MBoxDomain world) throws RemoteException {
        service.register(world);
    }

    @Override
    public void update(MBoxDomain world) throws RemoteException {}

    @Override
    public MBoxSchedRes schedule(MBoxSchedReq req) throws RemoteException {
        return service.schedule(req);
    }

    @Override
    public void indicate(MBoxSchedInfo info) throws RemoteException {
        service.indicate(info);
    }
}
