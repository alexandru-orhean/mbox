package core.service;

import core.common.*;
import org.workflowsim.planning.HEFTPlanningAlgorithm;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 */
public interface MBoxSchedulerInterface extends Remote {

    void register(MBoxDomain domain) throws RemoteException;
    void update(MBoxDomain domain) throws RemoteException;
    MBoxSchedRes schedule(MBoxSchedReq request) throws RemoteException;
    void indicate(MBoxSchedInfo info) throws RemoteException;
}
