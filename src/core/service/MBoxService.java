package core.service;

import core.MBox;
import core.common.MBoxDomain;
import core.common.MBoxSchedInfo;
import core.common.MBoxSchedReq;
import core.common.MBoxSchedRes;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 */
public class MBoxService implements Runnable {
    private MBox box;
    private final String name = "Scheduler01";
    private Registry registry;

    public MBoxService(MBox box) {
        this.box = box;
    }

    public void register(MBoxDomain world) {
        box.register(world);
    }

    public MBoxSchedRes schedule(MBoxSchedReq req) {
        return box.schedule(req);
    }

    public void indicate(MBoxSchedInfo info) {
        box.indicate(info);
    }

    public void terminate() {
        try {
            registry.unbind(name);
            UnicastRemoteObject.unexportObject(registry, true);
        } catch (Exception e) {
            System.err.println("MBox service could not stop properly!");
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            MBoxScheduler scheduler = new MBoxScheduler(this);
            MBoxSchedulerInterface stub = (MBoxSchedulerInterface)
                    UnicastRemoteObject.exportObject(scheduler, 0);
            registry = LocateRegistry.createRegistry(12345);
            registry.bind(name, stub);
        } catch (Exception e) {
            System.err.println("MBox could not bind scheduler.");
            e.printStackTrace();
        }
    }
}
