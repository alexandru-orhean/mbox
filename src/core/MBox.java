package core;

import core.common.MBoxDomain;
import core.common.MBoxSchedInfo;
import core.common.MBoxSchedReq;
import core.common.MBoxSchedRes;
import core.ml.MLDatabase;
import core.ml.MLSupervisor;
import core.service.MBoxService;

import java.util.ArrayList;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 * MBox represents the main component in the application architecture. It holds
 * the rest of the components, assuring graceful initialization, communication
 * and termination of execution of structural components and functional threads.
 * The implementation respects the singleton and mediator design patterns.
 */
public class MBox {
    private static MBox instance = null;
    private MLSupervisor supervisor;
    private MLDatabase database;
    private MBoxService service;
    private MBoxUI ui;
    private ArrayList<Thread> threadList;

    private MBox() {
        supervisor = new MLSupervisor();
        database = new MLDatabase();
        service = new MBoxService(this);
        ui = new MBoxUI(this);
        threadList = new ArrayList<>();
    }

    public static MBox getInstance() {
        if (instance == null) {
            instance = new MBox();
        }

        return instance;
    }

    /**
     * Starts the structural components and functional threads of the
     * application and waits for their graceful termination.
     */
    public void run() {
        System.out.println("Starting core.MBox ...");

        threadList.add(new Thread(ui));
        threadList.add(new Thread(service));

        for (Thread t : threadList) {
            t.start();
        }

        for (Thread t : threadList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Terminating core.MBox ...");
        System.exit(0);
    }

    /**
     * Terminates the execution of all structural components.
     */
    public void terminate() {
        ui.terminate();
        service.terminate();
    }

    public void register(MBoxDomain world) {}

    public MBoxSchedRes schedule(MBoxSchedReq req) {
        return null;
    }

    public void indicate(MBoxSchedInfo info) {}

    public static void main(String[] args) {
        MBox app = MBox.getInstance();
        app.run();
    }
}
