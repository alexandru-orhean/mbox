package core.common;

import java.io.Serializable;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 * MBoxSchedRes incorporates the scheduling plan of the requested tasks and the
 * information regarding the status of the response.
 */
public class MBoxSchedRes implements Serializable {
    private MBoxPlanStatus status;
    private MBoxPlan plan;

    public MBoxSchedRes(MBoxPlanStatus status, MBoxPlan plan) {
        this.status = status;
        this.plan = plan;
    }

    public MBoxPlanStatus getStatus() {
        return status;
    }

    public MBoxPlan getPlan() {
        return plan;
    }
}
