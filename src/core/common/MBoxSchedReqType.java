package core.common;

/**
 * Created by Alexandru Iulian Orhean on 6/21/16.
 */
public enum MBoxSchedReqType {
    FAST,
    LEARN,
}
