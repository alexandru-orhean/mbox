package core.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 * MBoxPlan incorporates the actual scheduling of the task given to the remote
 * scheduler, represented as a dictionary of nodes as keys and lists of tasks
 * as values.
 */
public class MBoxPlan implements Serializable {
    private HashMap<MBoxNode, ArrayList<MBoxTask>> plan;

    public MBoxPlan() {
        plan = new HashMap<>();
    }

    public void addNode(MBoxNode node) {
        plan.put(node, new ArrayList<>());
    }

    public void addTask(MBoxNode node, MBoxTask task) {
        if (!plan.containsKey(node)) {
            addNode(node);
        }

        plan.get(node).add(task);
    }

    public HashMap<MBoxNode, ArrayList<MBoxTask>> getPlan() {
        return plan;
    }
}
