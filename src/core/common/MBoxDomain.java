package core.common;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 * MBoxDomain represents the machine learning world in which the scheduler
 * agent will make actions and determine a scheduling plan. This class must
 * contain all the relevant information regarding the cluster and it will be
 * registered in the mbox at local scheduler initialization.
 */
public class MBoxDomain implements Serializable {
    private Integer identifier;
    private Integer precision;
    private ArrayList<MBoxNode> nodeList;

    public MBoxDomain(Integer identifier, Integer precision) {
        this.identifier = identifier;
        this.precision = precision;
        nodeList = new ArrayList<>();
    }

    public Integer getIdentifier() {
        return identifier;
    }

    public Integer getPrecision() {
        return precision;
    }

    public void addNode(MBoxNode node) {
        nodeList.add(node);
    }

    public ArrayList<MBoxNode> getNodeList() {
        return nodeList;
    }
}
