package core.common;

import java.io.Serializable;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 * MBoxSchedInfo incorporates the necessary information used in determining
 * the reward of the returned scheduling plan.
 */
public class MBoxSchedInfo implements Serializable {
    private Integer domainIndentifier;
    private Double elapsedTime;

    public MBoxSchedInfo(int domainIndentifier, Double elapsedTime) {
        this.domainIndentifier = domainIndentifier;
        this.elapsedTime = elapsedTime;
    }

    public Double getElapsedTime() {
        return elapsedTime;
    }

    public Integer getDomainIdentifier() {
        return this.domainIndentifier;
    }
}
