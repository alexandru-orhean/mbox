package core.common;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 */
public class MBoxSchedReq implements Serializable {
    private Integer domainIdentifier;
    private MBoxSchedReqType type;
    private ArrayList<MBoxTask> taskList;

    public MBoxSchedReq(int domainIdentifier, ArrayList<MBoxTask> taskList) {
        this.domainIdentifier = domainIdentifier;
        this.type = MBoxSchedReqType.FAST;
        this.taskList = taskList;
    }

    public void setType(MBoxSchedReqType type) {
        this.type = type;
    }

    public MBoxSchedReqType getType() {
        return type;
    }

    public ArrayList<MBoxTask> getTaskList() {
        return taskList;
    }

    public Integer getDomainIdentifier() {
        return domainIdentifier;
    }
}
