package core.common;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 * MBoxTask incorporates the basic information regarding a task, from a
 * list of tasks that form a cluster job: a unique identifier inside the job
 * graph, a list of parent tasks, and a list of sibling tasks, that could be
 * run in parallel.
 */
public class MBoxTask implements Serializable {
    private Integer identifier;
    private ArrayList<Integer> parentList;
    private ArrayList<Integer> siblingList;

    public MBoxTask(Integer identifier) {
        this.identifier = identifier;
        parentList = new ArrayList<>();
        siblingList = new ArrayList<>();
    }

    public Integer getIdentifier() {
        return identifier;
    }

    public void addParent(Integer identifier) {
        parentList.add(identifier);
    }

    public ArrayList<Integer> getParents() {
        return parentList;
    }

    public void addSibling(Integer identifier) {
        siblingList.add(identifier);
    }

    public ArrayList<Integer> getSiblings() {
        return siblingList;
    }
}
