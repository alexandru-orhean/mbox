package core.common;

/**
 * Created by Alexandru Iulian Orhean on 6/21/16.
 * MBoxNodeState defines the state of a node by the computational load.
 */
public enum MBoxNodeState {
    NOLOAD,
    LOWLOAD,
    HIGHLOAD,
    FULLLOAD,
}
