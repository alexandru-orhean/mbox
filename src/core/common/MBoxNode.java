package core.common;

import java.io.Serializable;

/**
 * Created by Alexandru Iulian Orhean on 6/14/16.
 * MBoxNode incorporates the basic information regarding a physical node
 * of a cluster.
 */
public class MBoxNode implements Serializable {
    private Integer identifier;
    private MBoxNodeState state;

    public MBoxNode(Integer identifier) {
        this.identifier = identifier;
        this.state = MBoxNodeState.NOLOAD;
    }

    public void setState(MBoxNodeState state) {
        this.state = state;
    }

    public MBoxNodeState getState() {
        return state;
    }

    public Integer getIdentifier() {
        return identifier;
    }

}
