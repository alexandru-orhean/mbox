package core.common;

import org.omg.PortableInterceptor.SUCCESSFUL;

/**
 * Created by Alexandru Iulian Orhean on 6/21/16.
 */
public enum MBoxPlanStatus {
    FAILED,
    SUCCESSFUL,
    LEARNING,
}
