package example;

import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.core.CloudSim;
import org.workflowsim.*;
import org.workflowsim.utils.ClusteringParameters;
import org.workflowsim.utils.OverheadParameters;
import org.workflowsim.utils.Parameters;
import org.workflowsim.utils.ReplicaCatalog;

import java.io.File;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Alexandru Iulian Orhean on 6/23/16.
 */
public class WorkflowSimDHEFT extends WorkflowSimHEFT {
    public static void main(String[] args) {
        try {
            String daxPath = "/home/fenrir/universe/thesis/workspace/" +
                    "WorkflowSim-1.0/config/dax/Montage_25.xml";
            File daxFile = new File(daxPath);
            if (!daxFile.exists()) {
                Log.printLine("Wrong daxFile!!!");
                return;
            }

            Parameters.SchedulingAlgorithm sch_method =
                    Parameters.SchedulingAlgorithm.STATIC;
            Parameters.PlanningAlgorithm pln_method =
                    Parameters.PlanningAlgorithm.DHEFT;
            ReplicaCatalog.FileSystem file_system =
                    ReplicaCatalog.FileSystem.LOCAL;

            OverheadParameters op = new OverheadParameters(0, null, null, null,
                    null, 0);

            ClusteringParameters.ClusteringMethod method =
                    ClusteringParameters.ClusteringMethod.NONE;

            ClusteringParameters cp = new ClusteringParameters(0, 0, method,
                    null);

            Parameters.init(vmNumber, daxPath, null, null, op, cp, sch_method,
                    pln_method, null, 0);
            ReplicaCatalog.init(file_system);

            int num_user = 1;
            Calendar calendar = Calendar.getInstance();
            boolean trace_flag = false;

            CloudSim.init(num_user, calendar, trace_flag);

            WorkflowDatacenter datacenter = createDatacenter("datacenter");
            WorkflowPlanner wfPlanner = new WorkflowPlanner("planner", 1);
            WorkflowEngine wfEngine = wfPlanner.getWorkflowEngine();
            List<CondorVM> vmList = createVM(wfEngine.getSchedulerId(0),
                    Parameters.getVmNum());
            wfEngine.submitVmList(vmList, 0);
            wfEngine.bindSchedulerDatacenter(datacenter.getId(), 0);

            CloudSim.startSimulation();
            List<Job> outputList = wfEngine.getJobsReceivedList();
            CloudSim.stopSimulation();
            printJobList(outputList);
        } catch (Exception e) {
            e.printStackTrace();
            Log.printLine("Simulation terminated due to unexpected error!!!");
        }
    }
}
