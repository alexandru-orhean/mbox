package example;

import core.MBox;
import core.common.*;
import core.service.MBoxSchedulerInterface;
import org.cloudbus.cloudsim.Log;
import org.workflowsim.CondorVM;
import org.workflowsim.Task;
import org.workflowsim.planning.RandomPlanningAlgorithm;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;

/**
 * Created by Alexandru Iulian Orhean on 6/15/16.
 */
public class MyScheduler extends RandomPlanningAlgorithm {
    protected final static int learningEpisodes = 100;
    protected MBoxSchedulerInterface scheduler = null;

    public MyScheduler() {
        try {
            String name = "Scheduler01";
            Registry registry = LocateRegistry.getRegistry(12345);
            scheduler = (MBoxSchedulerInterface) registry.lookup(name);
        } catch (Exception e) {
            e.printStackTrace();
            Log.printLine("MBox Scheduler error!");
        }
    }

    protected MBoxDomain getDomain(ArrayList<CondorVM> vms) {
        MBoxDomain domain = new MBoxDomain(0, 10);

        for (int i = 0; i < vms.size(); i++) {
            domain.addNode(new MBoxNode(i));
        }

        return domain;
    }

    protected MBoxSchedReq getRequest(ArrayList<Task> tasks) {
        ArrayList<MBoxTask> taskList = new ArrayList<>();

        for (int i = 0; i < tasks.size(); i++) {
            MBoxTask task = new MBoxTask(i);

            for (Task t1 : tasks.get(i).getParentList()) {
                task.addParent(tasks.indexOf(t1));

                HashSet<Integer> siblings = new HashSet<>();
                for (Task t2 : t1.getChildList()) {
                    int j = tasks.indexOf(t2);
                    if (j != i) {
                        siblings.add(j);
                    }
                }

                for (Integer j : siblings) {
                    task.addSibling(j);
                }
            }

            taskList.add(task);
        }

        return new MBoxSchedReq(0, taskList);
    }

    protected void schedule(MBoxPlan plan, ArrayList<Task> tasks,
                            ArrayList<CondorVM> vms) {
        HashMap<MBoxNode, ArrayList<MBoxTask>> map = plan.getPlan();

        for (MBoxNode node : map.keySet()) {
            int vmId = vms.get(node.getIdentifier()).getId();
            for (MBoxTask task : map.get(node)) {
                tasks.get(task.getIdentifier()).setVmId(vmId);
            }
        }
    }

    @Override
    public void run() {
        if (scheduler == null) {
            Log.printLine("MBox Scheduler was not properly initialized.");
            super.run();
        }
        try {
            scheduler.register(getDomain((ArrayList<CondorVM>) getVmList()));
            MBoxSchedReq req = getRequest((ArrayList<Task>) getTaskList());
            req.setType(MBoxSchedReqType.LEARN);
            MBoxSchedRes res = scheduler.schedule(req);
            if (res == null) {
                Log.printLine("MBox Scheduler error, running random planning.");
                super.run();
            } else {
                schedule(res.getPlan(), (ArrayList<Task>) getTaskList(),
                        (ArrayList <CondorVM>) getVmList());
            }
        } catch (Exception e){
            e.printStackTrace();
            Log.printLine("MBox Scheduler error, running random planning.");
            super.run();
        }
    }

    public void indicate(double elapsedTime) {
        MBoxSchedInfo info = new MBoxSchedInfo(0, elapsedTime);
        try {
            scheduler.indicate(info);
        } catch (RemoteException e) {
            e.printStackTrace();
            Log.printLine("MBox Scheduler error, could not be notified.");
        }
    }
}
