package example;

import java.io.File;
import java.text.DecimalFormat;
import java.util.*;

import org.cloudbus.cloudsim.*;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;
import org.omg.CORBA.ARG_IN;
import org.workflowsim.CondorVM;
import org.workflowsim.Job;
import org.workflowsim.Task;
import org.workflowsim.WorkflowDatacenter;
import org.workflowsim.WorkflowEngine;
import org.workflowsim.WorkflowPlanner;
import org.workflowsim.utils.ClusteringParameters;
import org.workflowsim.utils.OverheadParameters;
import org.workflowsim.utils.Parameters;
import org.workflowsim.utils.ReplicaCatalog;

/**
 * Created by Alexandru Iulian Orhean on 6/22/16.
 */
public class WorkflowSimHEFT {
    protected static final int vmNumber = 10;
    protected static final int hostNumber = 10;
    protected static final int discrepancy = 4;

    protected static List<CondorVM> createVM(int userId, int vms) {
        LinkedList<CondorVM> list = new LinkedList<>();

        long size = 10000;
        int ram = 256;
        int mips = 500;
        long bw = 500;
        int pesNumber = 1;
        String vmm = "Xen";

        CondorVM[] vm = new CondorVM[vms];
        for (int i = 0; i < vms; i++) {
            double ratio = ((i * discrepancy) / vms) + 1;
            vm[i] = new CondorVM(i, userId, (mips * ratio), pesNumber, ram,
                    (long) (bw * ratio), size, vmm,
                    new CloudletSchedulerSpaceShared());
            list.add(vm[i]);
        }

        return list;
    }

    protected static WorkflowDatacenter createDatacenter(String name) {
        List<Host> hostList = new ArrayList<>();

        int nrpe = 2;
        double mips = 2000;
        int ram = 2024;
        long storage = 1000000;
        int bw = 10000;
        for (int i = 0; i <= hostNumber; i++) {
            List<Pe> peList = new ArrayList<>();
            for (int j = 0; j < nrpe; j++) {
                peList.add(new Pe(j, new PeProvisionerSimple(mips)));
            }
            hostList.add(new Host(i, new RamProvisionerSimple(ram),
                    new BwProvisionerSimple(bw), storage, peList,
                    new VmSchedulerSpaceShared(peList)));
        }

        String arch = "x86";
        String os = "Linux";
        String vmm = "Xen";
        double time_zone = 10.0;
        double cost = 3.0;
        double costPerMem = 0.05;
        double costPerStorage = 0.1;
        double costPerBw = 0.1;
        LinkedList<Storage> storageList = new LinkedList<>();
        WorkflowDatacenter datacenter = null;

        DatacenterCharacteristics characteristics =
                new DatacenterCharacteristics(arch, os, vmm, hostList,
                        time_zone, cost, costPerMem, costPerStorage, costPerBw);

        int maxTransferRate = 15;

        try {
            HarddriveStorage s = new HarddriveStorage(name, 1e12);
            s.setMaxTransferRate(maxTransferRate);
            storageList.add(s);
            datacenter = new WorkflowDatacenter(name, characteristics,
                    new VmAllocationPolicySimple(hostList), storageList, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return datacenter;
    }

    protected static void printJobList(List<Job> list) {
        String indent = "\t";
        double totalTime = 0;
        Log.printLine();
        Log.printLine("========== OUTPUT ==========");
        Log.printLine("Job ID" + indent + "Task ID" + indent + "STATUS" + indent
                + "Data center ID" + indent + "VM ID" + indent + indent
                + "Time" + indent + "Start Time" + indent + "Finish Time"
                + indent + "Depth");
        DecimalFormat dft = new DecimalFormat("###.##");
        for (Job job : list) {
            Log.print(indent + job.getCloudletId() + indent + indent);
            if (job.getClassType() == Parameters.ClassType.STAGE_IN.value) {
                Log.print("Stage-in");
            }
            for (Task task : job.getTaskList()) {
                Log.print(task.getCloudletId() + ",");
            }
            Log.print(indent);

            if (job.getCloudletStatus() == Cloudlet.SUCCESS) {
                Log.print("SUCCESS");
                Log.printLine(indent + indent + job.getResourceId() + indent
                        + indent + indent + job.getVmId() + indent + indent
                        + indent + dft.format(job.getActualCPUTime()) + indent
                        + indent + dft.format(job.getExecStartTime()) + indent
                        + indent + indent + dft.format(job.getFinishTime())
                        + indent + indent + indent + job.getDepth());
            } else if (job.getCloudletStatus() == Cloudlet.FAILED) {
                Log.print("FAILED");
                Log.printLine(indent + indent + job.getResourceId() + indent
                        + indent + indent + job.getVmId() + indent + indent
                        + indent + dft.format(job.getActualCPUTime()) + indent
                        + indent + dft.format(job.getExecStartTime()) + indent
                        + indent + indent + dft.format(job.getFinishTime())
                        + indent + indent + indent + job.getDepth());
            }

            totalTime += job.getActualCPUTime();
        }
        Log.printLine("========== CUSTOM ==========");
        Log.printLine("Total elapsed time: " + totalTime);
    }

    public static void main(String[] args) {
        try {
            String daxPath = "/home/fenrir/universe/thesis/workspace/" +
                    "WorkflowSim-1.0/config/dax/Montage_25.xml";
            File daxFile = new File(daxPath);
            if (!daxFile.exists()) {
                Log.printLine("Wrong daxFile!!!");
                return;
            }

            Parameters.SchedulingAlgorithm sch_method =
                    Parameters.SchedulingAlgorithm.STATIC;
            Parameters.PlanningAlgorithm pln_method =
                    Parameters.PlanningAlgorithm.HEFT;
            ReplicaCatalog.FileSystem file_system =
                    ReplicaCatalog.FileSystem.LOCAL;

            OverheadParameters op = new OverheadParameters(0, null, null, null,
                    null, 0);

            ClusteringParameters.ClusteringMethod method =
                    ClusteringParameters.ClusteringMethod.NONE;

            ClusteringParameters cp = new ClusteringParameters(0, 0, method,
                    null);

            Parameters.init(vmNumber, daxPath, null, null, op, cp, sch_method,
                    pln_method, null, 0);
            ReplicaCatalog.init(file_system);

            int num_user = 1;
            Calendar calendar = Calendar.getInstance();
            boolean trace_flag = false;

            CloudSim.init(num_user, calendar, trace_flag);

            WorkflowDatacenter datacenter = createDatacenter("datacenter");
            WorkflowPlanner wfPlanner = new WorkflowPlanner("planner", 1);
            WorkflowEngine wfEngine = wfPlanner.getWorkflowEngine();
            List<CondorVM> vmList = createVM(wfEngine.getSchedulerId(0),
                    Parameters.getVmNum());
            wfEngine.submitVmList(vmList, 0);
            wfEngine.bindSchedulerDatacenter(datacenter.getId(), 0);

            CloudSim.startSimulation();
            List<Job> outputList = wfEngine.getJobsReceivedList();
            CloudSim.stopSimulation();
            printJobList(outputList);
        } catch (Exception e) {
            e.printStackTrace();
            Log.printLine("Simulation terminated due to unexpected error!!!");
        }
    }
}
