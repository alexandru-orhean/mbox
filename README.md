Project name: Machine Learning Box for Scheduling Tasks in Distributed Systems

Owner: Alexandru Iulian Orhean (alexandru.orhean@outlook.com)

depends on: Argparse4j (https://argparse4j.github.io), BURLAP (http://burlap.cs.brown.edu/), WorkflowSim (http://www.workflowsim.org/index.html)